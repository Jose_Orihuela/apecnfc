<?php 
	
 	require_once("lib/nusoap.php");
	require_once("funciones_ws.php");  

	
	$namespace = "http://localhost/pruebaws/servidor_ws.php";
	$server = new soap_server();
	$server->configureWSDL("ServerNFC");
	$server->wsdl->schemaTargetNamespace = $namespace;

	$server->register(
                // method name:
                'miMetodo',
                // parameter list:
                array('name'=>'xsd:string'), 
                // return value(s):
                array('return'=>'xsd:string'),
                // namespace:
                $namespace,
                // soapaction: (use default)
                false,
                // style: rpc or document
                'rpc',
                // use: encoded or literal
                'encoded',
                // description: documentation for the method
                'radical');


	// Get our posted data if the service is being consumed
	// otherwise leave this data blank.                
	$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) 
	                ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';

	// pass our posted data (or nothing) to the soap service                    
	$server->service($POST_DATA);   

	exit();
?>