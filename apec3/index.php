<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title>APEC</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet"	href="css/encabezado.css">
</head>

<script src="http://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/datatable/js/jquery.dataTables.min.js"></script>
<script src="js/datatable/js/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet"  href="js/datatable/css/dataTables.bootstrap.min.css">


<script>
	$(function () {
	    $('#table_id').DataTable({
	    	language: {
				    "decimal":        "",
				    "emptyTable":     "No hay datos en la tabla",
				    "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
				    "infoEmpty":      "Mostrando 0 a 0 de 0 entradas",
				    "infoFiltered":   "(filtrando de _MAX_ total de entradas)",
				    "infoPostFix":    "",
				    "thousands":      ",",
				    "lengthMenu":     "Mostrar _MENU_ entradas",
				    "loadingRecords": "Cargando...",
				    "processing":     "Procesando...",
				    "search":         "Buscar:",
				    "zeroRecords":    "No se encontraron registros coincidentes",
				    "paginate": {
				        "first":      "Primero",
				        "last":       "Ultimo",
				        "next":       "Siguiente",
				        "previous":   "Anterior"
				    },
				    "aria": {
				        "sortAscending":  ": Activar para ordenar la columna ascendente",
				        "sortDescending": ": activar para ordenar la columna Descendente"
				    }
			}
		    
	    });
	} );
</script>
<body>
<?php include("inc_encabezado.php");?>
<br></br>
<br></br>
<div class="container">
<div class="row">
<div class="col-sm-12">
<h1 style="text-align:center">Registro de Tarjetas RFID y NFC</h1>
</div>
</div>
</div>

<br></br>
<br></br>

<div class="container">
 <div class="row">
 <div class="col-sm-12">
 <?php include('servido.php');?>

	<table class="table table-bordered"  id="table_id" class="display">
			<thead>
				<tr>
				    <th style="text-align:center">ITEM</th>
					<th style="text-align:center">NOMBRE Y APELLIDO</th>
					<th style="text-align:center">RFID</th>
					<th style="text-align:center">NFC </th>
				</tr>
			</thead>
			
		
		<tbody>
		<?php for ($i=0;$i<8;$i++){?> 
		 <tr>		<input type="hidden" class="hidden-id" name="id" value="<?php echo $i+1; ?>"/>
					<td style="text-align:center"><?php echo $i+1 ; ?></td>
					<td style="text-align:center"><?php echo devolver($i) ; ?></td>
					<td style="text-align:center"><button type="submit" onClick="this.disabled='disabled'" class="btn btn-warning">RFID</button></td>
					<td style="text-align:center"><button type="button" class="btn btn-success btn-NFC">NFC</button></td>
		</tr>
		<?php } ?> 

			</tbody> 
	
	</table>
  
  </div>
</div>
</div>
  

  
    <!-- footer -->
	
<?php include("inc_pie_pagina.php");?>
	
<script>
		$(".btn-NFC").click(function(){
		    var total_id = $(this).closest('tr').find('.hidden-id').val();
		    var dataParticipante = {
        		"action": total_id
        	};    	
        	 $.ajax({
              type: "POST",
              url: "nfc/prueba.php,
              data: dataParticipante,
              dataType: "json",
              contentType: "application/json; charset=utf-8",      
              success: function(data){
                console.log(data["message"]);
            
              }
            });    

		});


    
        

    </script>
</body>