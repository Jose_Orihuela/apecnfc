package com.ja.bcard.app.ui.fragments;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ja.bcard.app.R;
import com.ja.bcard.app.data.entity.Card;
import com.ja.bcard.app.data.entity.Cards;
import com.ja.bcard.app.util.Utils;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
public class EditCardFragment extends BaseFragment{

    @Bind( R.id.card_holder_name )
    protected EditText holderName;
    @Bind( R.id.card_holder_role )
    protected EditText holderRole;
    @Bind( R.id.card_holder_phone)
    protected EditText holderPhone;
    @Bind( R.id.card_holder_email)
    protected EditText holderEmail;
    @Bind( R.id.card_holder_company)
    protected EditText holderCompany;
    @Bind( R.id.card_holder_address)
    protected EditText holderAddress;
    @Bind( R.id.fab)
    protected FloatingActionButton fab;

    private boolean mCardSaved;
    private Card card;
    private NfcAdapter mAdapter;
    private boolean mWriteMode;
    private PendingIntent mPendingIntent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_card,container,false );
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }

    @OnClick( R.id.fab )
    public void onActionClick( View v){
        if( mCardSaved && null != card )
            writeCardToTag();
        else{
            if( !validInputs() ){
                Toast.makeText(getActivity(),"Please complete empty fields",Toast.LENGTH_LONG).show();
                return;
            }
            card = new Card();

            card.setColor(Utils.pickCardColor(getActivity()));
            card.setOwn(true);
            card.setName( holderName.getText().toString() );
            card.setRole( holderRole.getText().toString() );
            card.setPhone( holderPhone.getText().toString() );
            card.setEmail( holderEmail.getText().toString() );
            card.setCompany( holderCompany.getText().toString() );
            card.setAddress(holderAddress.getText().toString());
            if( mCardSaved = Cards.save(getActivity(),card)){
                Toast.makeText( getActivity(),"Card saved successfully.!",Toast.LENGTH_LONG).show();
                fab.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ic_nfc ));
                mCardSaved = true;
            }
        }
    }

    private boolean validInputs(){
        boolean result = true;
        result &= !Utils.isEmpty( holderName.getText() );
        result &= !Utils.isEmpty( holderRole.getText() );
        result &= !Utils.isEmpty(holderPhone.getText());
        result &= !Utils.isEmpty( holderEmail.getText());
        result &= !Utils.isEmpty( holderCompany.getText() );
        result &= !Utils.isEmpty( holderAddress.getText() );
        return result;
    }

    @Override
    public void onPause() {
        super.onPause();
        if( null != mAdapter )
            mAdapter.disableForegroundDispatch(getActivity());
    }

    private void writeCardToTag(){
        Log.e( "******","trying to write to tag" );
        if( null == mAdapter )
            mAdapter = NfcAdapter.getDefaultAdapter(getActivity());
        if( null == mAdapter ) {
            Toast.makeText(getActivity(), "This device isn't NFC compatible", Toast.LENGTH_LONG).show();
            return;
        }else {
            mWriteMode = true;
            mPendingIntent = PendingIntent.getActivity(getActivity(), 0, new Intent(getActivity(), getActivity().getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            final IntentFilter filter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            mAdapter.enableForegroundDispatch( getActivity(),mPendingIntent,new IntentFilter[]{ filter },null );
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        if( mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals( intent.getAction() ) ){
            Toast.makeText(getActivity(),"trying to write aar "+ getActivity().getApplication().getPackageName(),Toast.LENGTH_LONG).show();
            Tag detected = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            try {
                byte[] data = Utils.stringify(card).getBytes();
                Log.e( "****",""+ Arrays.toString(data)+", "+data.length );
//                byte[] data = Utils.getBytes(card);
                final NdefRecord mimeRecord = NdefRecord.createMime("application/serializable", data);
                final NdefRecord aar = NdefRecord.createApplicationRecord(getActivity().getApplication().getPackageName());

                String nameVcard = "BEGIN:VCARD" +"\n"+ "VERSION:2.1" +"\n" + "N:Hilda;" + "\n" +"ORG: PlanAyala"+"\n"+ "TEL;HOME:6302421" +"\n"+ "END:VCARD";
                byte[] uriField = nameVcard.getBytes(Charset.forName("US-ASCII"));
                byte[] payload = new byte[uriField.length + 1];              //add 1 for the URI Prefix
                //payload[0] = 0x01;                                      //prefixes http://www. to the URI
                System.arraycopy(uriField, 0, payload, 1, uriField.length);  //appends URI to payload

                //NdefRecord nfcRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, "text/vcard".getBytes(), new byte[0], payload);

                NdefRecord record = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,"text/vcard".getBytes(), new byte[] {}, uriField);

                if (Utils.writeTag(getActivity(),new NdefMessage(record), detected)) {
                    Toast.makeText(getActivity(), "Card written to tag successfully", Toast.LENGTH_LONG).show();
                }
            }catch ( Exception ex){
                ex.printStackTrace();
                Toast.makeText(getActivity(), "Card wasn't written to tag", Toast.LENGTH_LONG).show();
            }
            mWriteMode = false;
        }
    }
}
