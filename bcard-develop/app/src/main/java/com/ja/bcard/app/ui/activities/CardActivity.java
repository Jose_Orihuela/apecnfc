package com.ja.bcard.app.ui.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.ja.bcard.app.ui.fragments.BaseFragment;
import com.ja.bcard.app.ui.fragments.EditCardFragment;
import com.ja.bcard.app.ui.fragments.SearchCardFragment;
import com.ja.bcard.app.ui.fragments.ViewCardFragment;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */

public class CardActivity extends Activity {
    public static final String EXTRA_CARD = "CardActivity.EXTRA_CARD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment fragment;
        //if (Intent.ACTION_EDIT.equals(getIntent().getAction()))
         //   fragment = new EditCardFragment();
        //else if (Intent.ACTION_VIEW.equals(getIntent().getAction()))
          //  fragment = new ViewCardFragment();
        //else
            fragment = new SearchCardFragment();

        fragment.setArguments(getIntent().getExtras());
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(android.R.id.content, fragment,"CardFragment").commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Fragment fr;
        if( null != (fr = getFragmentManager().findFragmentByTag("CardFragment"))){
            if( fr instanceof BaseFragment )
                ((BaseFragment)fr).onNewIntent(intent);
            Log.e( "******","Passing NFC Intent to actual fragment" );
        }
    }
}
