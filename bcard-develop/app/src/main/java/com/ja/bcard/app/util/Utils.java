package com.ja.bcard.app.util;

import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.util.Log;
import android.widget.Toast;
import com.ja.bcard.app.R;
import com.ja.bcard.app.data.entity.Card;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
public final class Utils {
    static Random r;

    public static final int pickCardColor(Context context) {
        int[] colors = context.getResources().getIntArray(R.array.card_colors);
        if (null == r)
            r = new Random( ((int)new Date().getTime()) / colors.length );
        int idx = r.nextInt(colors.length) % colors.length;
        return colors[idx];
    }

    public static byte[] getBytes(Object object) throws IOException {
        byte[] data = new byte[]{};
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(object);
            data = bos.toByteArray();
            bos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return data;
    }

    public static Object fromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        Object o = null;
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInput in = new ObjectInputStream(bis);
            o = in.readObject();
        }catch ( Exception ex ){
            ex.printStackTrace();
        }
        return o;
    }


    public static final String stringify( Card card ){
        return card.toString();
    }

    public static final Card destringify( byte[] card ){
        String[] parts = new String( card ).split( ";" );
        Card c = new Card();
        c.setName(parts[0]);
        c.setEmail( parts[1] );
        c.setPhone( parts[2] );
        c.setAddress( parts[3] );
        c.setCompany( parts[4] );
        c.setOwn( new Boolean(parts[5]));
        c.setRole( parts[6] );
        try {
            c.setColor(Integer.parseInt(parts[7]));
        }catch ( Exception ex) {
            ex.printStackTrace();
            c.setColor( R.color._1 );
        }
        return  c;
    }


    public static boolean writeTag( Context context,NdefMessage msg, Tag ntag ){
        final int size = msg.toByteArray().length;
        try{
            Ndef ndef = Ndef.get(ntag);
            if( null != ndef ){
                ndef.connect();
                if( !ndef.isWritable() ){
                    Toast.makeText(context, "Tag isn't writable", Toast.LENGTH_LONG).show();
                    return false;
                }

                Log.e( "****","Tag space = " +ndef.getMaxSize()+", message size="+size);

                if( ndef.getMaxSize() < size ){
                    Toast.makeText(context,"Tag space too small",Toast.LENGTH_LONG).show();
                    return false;
                }
                ndef.writeNdefMessage(msg);
            }else{
                NdefFormatable formatable = NdefFormatable.get(ntag);
                if( null != formatable ){
                    try{
                        formatable.connect();
                        formatable.format(msg);
                    }catch ( IOException ex ){
                        ex.printStackTrace();
                        return false;
                    }
                }
            }
        }catch ( Exception ex ){
            ex.printStackTrace();
            return  false;
        }
        return true;
    }

    public static final boolean isEmpty( CharSequence cs ){
        return null == cs || "".equals(cs);
    }
}
