package com.ja.bcard.app.ui.fragments;

import android.app.Fragment;
import android.content.Intent;

/**
 * Created by Jansel Rodriguez on 4/10/2016.
 */
public class BaseFragment extends Fragment{
    public void onNewIntent( Intent intent ){}
}
