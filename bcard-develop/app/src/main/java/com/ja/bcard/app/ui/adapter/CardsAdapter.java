package com.ja.bcard.app.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.ja.bcard.app.R;
import com.ja.bcard.app.data.entity.Card;
import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
public class CardsAdapter extends BaseAdapter{

    private Context mContext;
    private List<Card> mCards;

    private LayoutInflater inflater;

    public CardsAdapter(Context context, List<Card> cards) {
        this.mContext = context;
        this.mCards = cards;
        inflater = LayoutInflater.from(context);
    }

    public List<Card> getCards() {
        return mCards;
    }

    public void setCards(List<Card> Cards) {
        this.mCards = Cards;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mCards.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if( null == view )
            view = inflater.inflate( R.layout.item_card,parent,false );

        final ViewHolder h;
        if( null == view.getTag()){
            h = new ViewHolder();
        }else{
            h = (ViewHolder) view.getTag();
        }
        ButterKnife.bind(h,view);
        final Card card = mCards.get(position);
        if( null != card ){
            h.headerContainer.setBackgroundColor( card.getColor() );
            h.holderPhoneIcon.setBackgroundColor( card.getColor() );
            h.holderEmailIcon.setBackgroundColor( card.getColor() );
            h.holderAddressIcon.setBackgroundColor( card.getColor() );

            h.holderName.setText(card.getName() );
            h.holderRole.setText( card.getRole() );
            h.holderCompany.setText( card.getCompany() );
            h.holderPhone.setText( card.getPhone() );
            h.holderEmail.setText( card.getEmail() );
            h.holderAddress.setText( card.getAddress() );
        }
        return view;
    }

    public static final class ViewHolder{
        @Bind( R.id.header_container )
        public ViewGroup headerContainer;
        @Bind( R.id.card_holder_name)
        public TextView holderName;
        @Bind( R.id.card_holder_role )
        public TextView holderRole;
        @Bind( R.id.card_holder_company )
        public TextView holderCompany;
        @Bind( R.id.card_holder_phone_icon)
        public ImageView holderPhoneIcon;
        @Bind( R.id.card_holder_phone )
        public TextView holderPhone;
        @Bind( R.id.card_holder_email_icon)
        public ImageView holderEmailIcon;
        @Bind( R.id.card_holder_email )
        public TextView holderEmail;
        @Bind( R.id.card_holder_address_icon)
        public ImageView holderAddressIcon;
        @Bind( R.id.card_holder_address )
        public TextView holderAddress;
    }
}
