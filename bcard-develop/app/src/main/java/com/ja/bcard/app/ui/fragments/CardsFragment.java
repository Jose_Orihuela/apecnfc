package com.ja.bcard.app.ui.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ja.bcard.app.R;
import com.ja.bcard.app.data.entity.Card;
import com.ja.bcard.app.data.entity.Cards;
import com.ja.bcard.app.data.provider.CardContract;
import com.ja.bcard.app.ui.activities.CardActivity;
import com.ja.bcard.app.ui.adapter.CardsAdapter;
import com.ja.bcard.app.util.Utils;
import com.michaldrabik.tapbarmenulib.TapBarMenu;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
public class CardsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = CardsFragment.class.getCanonicalName();

    private static final int FLIP_SELECTION_CONTENT = 0;
    private static final int FLIP_SELECTION_EMPTY = 1;

    private static final int CARD_CREATE_TAG    = 0;
    private static final int CARD_DISCOVER_TAG  = 1;

    @Bind(R.id.refresh_layout)
    protected SwipeRefreshLayout refreshLayout;
    @Bind(R.id.list)
    protected GridView list;
    @Bind(R.id.card_content_flipper)
    protected ViewFlipper flipper;
    @Bind(R.id.cards_menu)
    protected TapBarMenu cardsMenu;

    //elementos añadidos por mi: APLICAR y IP
    @Bind(R.id.buttonAplicar)
    Button btnaplicar;
    @Bind(R.id.editIP)
    EditText IP;

    private List<Card> mCards;
    private CardsAdapter mCardsAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cards, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        btnaplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IP.getText().length()!=0){

                    //Apertura la vista Search
                    Intent intent = new Intent(getActivity(), CardActivity.class );
                    intent.setAction( Intent.ACTION_SEARCH );
                    intent.putExtra("ip",IP.getText().toString());
                    startActivity( intent );

                    //Enviamos la IP ingresada a la vista Search

                    /*Intent i =new Intent(getActivity(),SearchCardFragment.class);
                    i.putExtra("ip",IP.getText().toString());
                    startActivity(i);
                    */

                }else {
                    Toast.makeText(getActivity(), "Por favor ingrese un IP", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle( R.string.all_cards );

        refreshLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                onReadyRefreshLayout();
                refreshLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }


    @Override
    public void onRefresh() {
        getLoaderManager().restartLoader(0,null,this);
    }

    private void onReadyRefreshLayout() {
        if (null != refreshLayout) {
            refreshLayout.setColorSchemeColors(
                    R.color.refresh_schema_1,
                    R.color.refresh_schema_2,
                    R.color.refresh_schema_3,
                    R.color.refresh_schema_4
            );
            refreshLayout.setOnRefreshListener(this);
            refreshLayout.setRefreshing(true);

            list.setAdapter(mCardsAdapter = new CardsAdapter(getActivity(), Collections.EMPTY_LIST));
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final Card card = mCards.get( position );
                    Intent intent = new Intent(getActivity(), CardActivity.class );
                    intent.setAction( Intent.ACTION_VIEW );
                    intent.putExtra(CardActivity.EXTRA_CARD,card );
                    startActivity(intent);
                }
            });
            getLoaderManager().initLoader(0, null, this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), CardContract.CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        refreshLayout.setRefreshing(false);

        mCards = Cards.from(cursor);
//        mCards = dummyCards(getActivity());
        if (null == mCards || 0 >= mCards.size()) {
            flipSelection(FLIP_SELECTION_EMPTY);
        } else {
            mCardsAdapter.setCards(mCards);
            flipSelection(FLIP_SELECTION_CONTENT);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCardsAdapter.setCards(Collections.EMPTY_LIST);
    }

    private void flipSelection(int selection) {
        if (null == flipper)
            return;
        flipper.setAnimateFirstView(true);
        flipper.setDisplayedChild(selection);
    }
    /*
    @OnClick( R.id.card_create )
    public void onCreateCard( View view){
        if( cardsMenu.isOpened() )
            cardsMenu.toggle();

        Intent intent = new Intent(getActivity(), CardActivity.class );
        intent.setAction( Intent.ACTION_EDIT );
        startActivity( intent );
    }

    @OnClick( R.id.card_discover)
    public void onDiscoverCard( View view ){
        if( cardsMenu.isOpened() )
            cardsMenu.toggle();

        Intent intent = new Intent(getActivity(), CardActivity.class );
        intent.setAction( Intent.ACTION_SEARCH );
        startActivity( intent );
    }

    @OnClick(R.id.buttonAplicar)
    public void cambiarVista(){
        btnaplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CardActivity.class );
                intent.setAction( Intent.ACTION_SEARCH );
                startActivity( intent );
            }
        });
    }

    */
    public static final List<Card> dummyCards( Context context){
        Card c1 = new Card();
        Card c2 = new Card();
        Card c3 = new Card();
        Card c4 = new Card();
        Card c5 = new Card();

        c1.setOwn(true);
        c1.setName( "Jansel Valentin" );
        c1.setAddress("Republica Dominicana");
        c1.setColor(Utils.pickCardColor(context));
        c1.setPhone("000-000-0000");
        c1.setRole("CEO");
        c1.setEmail("test@test.com");
        c1.setCompany("Company 1");

        c2.setName( "Jansel Valentin.2" );
        c2.setAddress("Republica Dominicana");
        c2.setColor(Utils.pickCardColor(context));
        c2.setPhone("000-000-0002");
        c2.setRole("CEO");
        c2.setEmail("test2@test.com");
        c2.setCompany("Company 2");

        c3.setName( "Jansel Valentin.3" );
        c3.setAddress("Republica Dominicana");
        c3.setColor(Utils.pickCardColor(context));
        c3.setPhone("000-000-0003");
        c3.setRole("CEO");
        c3.setEmail("test3@test.com");
        c3.setCompany("Company 3");

        c4.setName( "Jansel Valentin.4" );
        c4.setAddress("Republica Dominicana");
        c4.setColor(Utils.pickCardColor(context));
        c4.setPhone("000-000-0004");
        c4.setRole("CEO");
        c4.setEmail("test4@test.com");
        c4.setCompany("Company 4");

        c5.setName( "Jansel Valentin.5" );
        c5.setAddress("Republica Dominicana");
        c5.setColor(Utils.pickCardColor(context));
        c5.setPhone("000-000-0005");
        c5.setRole("CEO");
        c5.setEmail("test5@test.com");
        c5.setCompany("Company 5");

        return Arrays.asList( c1,c2,c3,c4,c5 );
    }
}
