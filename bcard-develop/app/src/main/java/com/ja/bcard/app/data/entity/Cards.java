package com.ja.bcard.app.data.entity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.util.Log;
import android.widget.Toast;

import com.ja.bcard.app.data.provider.CardContract;
import com.ja.bcard.app.util.Utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */

public final class Cards {
    public static final List<Card> from(Cursor c) {
        final List<Card> cards = new LinkedList<Card>();
        if( null != c && c.moveToFirst() ){
            do{
                byte[] data = c.getBlob( c.getColumnIndex( CardContract.DATA ));
                try {
//                    final Card card = (Card) Utils.fromBytes(data);
                    final Card card = Utils.destringify(data);
                    cards.add(card);
                }catch ( Exception ex){
                    ex.printStackTrace();
                }
            }while( c.moveToNext() );
        }
        return cards;
    }

    public static final boolean save(Context context, Card card) {
        try {
//            byte[] data = Utils.getBytes(card);
            byte[] data = Utils.stringify(card).getBytes();
            return save(context,data);
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static final boolean save(Context context,byte[] data) {
        if (null == data || 0 >= data.length)
            return false;
        ContentResolver resolver = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CardContract.DATA,data );
        resolver.insert( CardContract.CONTENT_URI,values );
        return true;
    }

    public static final boolean write(Context context, Card card, Intent intent) {

            Toast.makeText(context,"Grabando",Toast.LENGTH_LONG).show();
            Tag detected = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            byte[] data = Utils.stringify(card).getBytes();
            Log.e( "****",""+ Arrays.toString(data)+", "+data.length );
            final NdefRecord mimeRecord = NdefRecord.createMime("application/serializable", data);
            //final NdefRecord aar = NdefRecord.createApplicationRecord(context.getApplication().getPackageName());

                String nombreApellido = card.getName();
                String telefono = card.getPhone();
                String institucion = card.getCompany();
                String cargo = card.getRole();
                String correo = card.getEmail();

                String nameVcard = "BEGIN:VCARD\n" +
                                    "VERSION:3.0\n" +
                                    "N:" + nombreApellido  + ";;;\n" +
                                    "EMAIL;TYPE=INTERNET:" + correo + "\n" +
                                    "ORG:" + institucion + "\n" +
                                    "TITLE:" + cargo + "\n" +
                                    "TEL;CELL:" + telefono + "\n" +
                                    "END:VCARD";

            //String nameVcard = "BEGIN:VCARD" +"\n"+ "VERSION:2.1" +"\n" + "N:Joel;" + "\n" +"ORG: PlanAyala"+"\n"+ "TEL;HOME:6302421" +"\n"+ "END:VCARD";
            //String nameVcard = "Bienvenidos";
            byte[] uriField = nameVcard.getBytes(Charset.forName("US-ASCII"));
            byte[] payload = new byte[uriField.length + 1];              //add 1 for the URI Prefix
            //payload[0] = 0x01;                                      //prefixes http://www. to the URI
            System.arraycopy(uriField, 0, payload, 1, uriField.length);  //appends URI to payload

            //NdefRecord nfcRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, "text/vcard".getBytes(), new byte[0], payload);

            NdefRecord record = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,"text/vcard".getBytes(), new byte[] {}, uriField);

           if (Utils.writeTag(context,new NdefMessage(record), detected)) {
                Toast.makeText(context, "Escritura realizada con éxito.", Toast.LENGTH_LONG).show();

            }

        return true;
    }
}
