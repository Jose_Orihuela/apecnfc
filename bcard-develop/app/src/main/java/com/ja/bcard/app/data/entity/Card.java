package com.ja.bcard.app.data.entity;

import java.io.Serializable;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
public class Card implements Serializable{
    private String name;
    private String email;
    private String phone;
    private String address;
    private String company;
    private boolean own;
    private String role;
    private int color;

    public Card() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOwn() {
        return own;
    }

    public void setOwn(boolean own) {
        this.own = own;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return name+";"+email+";"+phone+";"+address+";"+company+";"+own+";"+role+";"+color;
    }
}
