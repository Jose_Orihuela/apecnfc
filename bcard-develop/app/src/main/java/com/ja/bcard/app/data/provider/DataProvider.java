package com.ja.bcard.app.data.provider;

import com.tjeannin.provigen.InvalidContractException;
import com.tjeannin.provigen.ProviGenProvider;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */

public class DataProvider extends ProviGenProvider {
    public DataProvider() throws InvalidContractException {
        super(new Class[]{ CardContract.class } );
    }
}
