package com.ja.bcard.app.ui.fragments;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.ja.bcard.app.R;
import com.ja.bcard.app.data.entity.Card;
import com.ja.bcard.app.data.entity.Cards;
import com.ja.bcard.app.util.Utils;
import com.skyfishjy.library.RippleBackground;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.Buffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */

public class SearchCardFragment extends BaseFragment{

    @Bind( R.id.rippleButton )
    protected RippleBackground rippleButton;

    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private IntentFilter[] mFilters;
    private String[][] mTechList;
    StringBuilder sb = new StringBuilder();
    //private final String NAMESPACE = "http://192.168.42.7:80/apec3/nfc/servidor_ws.php";
    //private final String URL = "http://192.168.42.7:80/apec3/nfc/servidor_ws.php?wsdl";
    //private final String SOAP_ACTION = "http://192.168.42.7:80/apec3/nfc/servidor_ws.php/ServerNFC/miMetodo";
    //private final String METHOD_NAME = "miMetodo";

    private String fromCurrency = "Hilda";
    private String webResponse = "";
    private String ipServer = "";
    private String nombres;
    private String myString = "";
    private Thread thread;
    private Handler handler = new Handler();


    //@Bind(R.id.editIP)
    //EditText IP;

    Object resultString;

    private boolean mWriteMode=true;
    Card card;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_card,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rippleButton.startRippleAnimation();

        Bundle p= getActivity().getIntent().getExtras();
        ipServer = p.getString("ip");



        mAdapter = NfcAdapter.getDefaultAdapter(getActivity());
        mPendingIntent = PendingIntent.getActivity(getActivity(), 0, new Intent(getActivity(), getActivity().getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        //IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        Toast.makeText(getActivity(),"Buscando Tag =)..." + ipServer ,Toast.LENGTH_LONG).show();
        URL url = null;
        String urlString="";
        card = new Card();
        BufferedReader in=null;
        String[] infoSplit = null;
        try {
            char output='5';
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://"+ipServer+"/respuestaAndroid.php?value="+output);//output is the variable you used in your program
            httpClient.execute(httpPost);

            //urlString = "http://"+variable+":80/apec3/nfc/info.txt";
            //Log.e("------------------",variable);
            urlString = "http://"+ipServer+"/apec3/nfc/info.txt";
            url = new URL(urlString);
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            Log.i("*****", "doInBackground"+in.toString());
            String str;
            while ((str = in.readLine()) != null) {
               infoSplit = str.split(";");
                card.setName(infoSplit[0]);
                card.setPhone(infoSplit[0]);
                card.setCompany(infoSplit[0]);
                card.setRole(infoSplit[0]);
                card.setEmail(infoSplit[0]);
            }
            in.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        card.setColor(Utils.pickCardColor(getActivity()));

        //Cards.write(card);

        //mFilters = new IntentFilter[]{ ndef };
        //mTechList = new String[][]{ new String[]{NfcF.class.getName()} };
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mAdapter)
            mAdapter.enableForegroundDispatch(getActivity(), mPendingIntent, mFilters, null);
    }

    @Override
    public void onNewIntent( Intent intent) {
        super.onNewIntent(intent);

        //new readtextfile().execute("http://10.0.2.2:80/apec3/nfc/info.txt");
        /*card.setName(infoSplit[0]);
        card.setPhone(infoSplit[0]);
        card.setCompany(infoSplit[0]);
        card.setRole(infoSplit[0]);
        card.setEmail(infoSplit[0]);*/
        /*InputStream is = this.getResources().openRawResource(R.raw.info);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String data = null;
        try {
            while((data = reader.readLine()) != null){
                webResponse = data;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
        //Bundle p= getActivity().getIntent().getExtras();
        //ipServer = p.getString("ip");


        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            String url = "http://"+ipServer+"/apec3/info.txt";
            URL oracle = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                Log.e("******", inputLine);
                String[] infoSplit = inputLine.split(";");
                card.setName(infoSplit[0] + " " + infoSplit[1]);
                card.setPhone(infoSplit[2]);
                card.setCompany(infoSplit[3]);
                card.setRole(infoSplit[4]);
                card.setEmail(infoSplit[5]);


                in.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    Cards.write(getActivity(), card, intent);

    }





    @Override
    public void onPause() {
        super.onPause();
        if (null != mAdapter)
            mAdapter.disableForegroundDispatch(getActivity());
    }
}
