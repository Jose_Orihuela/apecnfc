package com.ja.bcard.app.data.provider;

import android.net.Uri;
import com.tjeannin.provigen.Constraint;
import com.tjeannin.provigen.ProviGenBaseContract;
import com.tjeannin.provigen.annotation.Column;
import com.tjeannin.provigen.annotation.ContentUri;
import com.tjeannin.provigen.annotation.Contract;
import com.tjeannin.provigen.annotation.NotNull;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
@Contract(version = 3)
public interface CardContract extends ProviGenBaseContract {

    @Column(Column.Type.BLOB)
    @NotNull(Constraint.OnConflict.ABORT)
    String DATA = "DATA";

//    @Column(Column.Type.TEXT)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String NAME = "NAME";
//
//    @Column( Column.Type.INTEGER)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String OWN = "OWN";
//
//    @Column(Column.Type.TEXT)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String ROLE = "ROLE";
//
//    @Column(Column.Type.TEXT)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String COMPANY = "COMPANY";
//
//    @Column(Column.Type.TEXT)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String EMAIL = "EMAIL";
//
//    @Column(Column.Type.TEXT)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String PHONE = "PHONE";
//
//    @Column(Column.Type.TEXT)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String ADDRESS = "ADDRESS";
//
//    @Column(Column.Type.INTEGER)
//    @NotNull(Constraint.OnConflict.ABORT)
//    String COLOR = "COLOR";

    @ContentUri
    final Uri CONTENT_URI = Uri.parse("content://com.ja.bcard.app/cards");
}
