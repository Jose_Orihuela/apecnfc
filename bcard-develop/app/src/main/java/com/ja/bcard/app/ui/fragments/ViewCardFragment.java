package com.ja.bcard.app.ui.fragments;

import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.graphics.drawable.RippleDrawable;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ja.bcard.app.R;
import com.ja.bcard.app.data.entity.Card;
import com.ja.bcard.app.ui.activities.CardActivity;
import com.ja.bcard.app.util.Utils;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.ObjectOutput;
import java.util.Arrays;

/**
 * Created by Jansel Rodriguez on 4/9/2016.
 */
public class ViewCardFragment extends BaseFragment {

    @Bind(R.id.card_holder_name)
    protected TextView holderName;

    @Bind(R.id.card_holder_role)
    protected TextView holderRole;

    @Bind(R.id.card_holder_phone)
    protected TextView holderPhone;

    @Bind(R.id.card_holder_email)
    protected TextView holderEmail;

    @Bind(R.id.card_holder_address)
    protected TextView holderAddress;

    @Bind(R.id.fab)
    protected FloatingActionButton fab;

    private Card card;
    private NfcAdapter mAdapter;
    private boolean mWriteMode;
    private PendingIntent mPendingIntent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_card, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (null == getArguments() || null == (card = (Card) getArguments().getSerializable(CardActivity.EXTRA_CARD)))
            getActivity().finish();

        displayCard(card);
    }

    @OnClick(R.id.fab)
    public void onActionClick(View v) {
        if( null != card && card.isOwn() )
            writeCardToTag( );
        else
            callToCardHolder(card);
    }

    @Override
    public void onPause() {
        super.onPause();
        if( null != mAdapter )
            mAdapter.disableForegroundDispatch(getActivity());
    }

    private void writeCardToTag( ){
        Log.e( "***","Trying to write tag" );

        if( null == mAdapter )
            mAdapter = NfcAdapter.getDefaultAdapter(getActivity());
        if( null == mAdapter ) {
            Toast.makeText(getActivity(), "This device isn't NFC compatible", Toast.LENGTH_LONG).show();
            return;
        }else {
            mWriteMode = true;
            mPendingIntent = PendingIntent.getActivity(getActivity(),0,new Intent(getActivity(),getActivity().getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
            final IntentFilter filter = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            mAdapter.enableForegroundDispatch( getActivity(),mPendingIntent,new IntentFilter[]{ filter },null );
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        if( mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals( intent.getAction() ) ){
            Toast.makeText(getActivity(),"trying to write aar "+ getActivity().getApplication().getPackageName(),Toast.LENGTH_LONG).show();
            Tag detected = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            try {
                byte[] data = Utils.stringify(card).getBytes();
                Log.e( "****",""+Arrays.toString(data)+", "+data.length );

                final NdefRecord mimeRecord = NdefRecord.createMime("application/serializable", data);
                final NdefRecord aar = NdefRecord.createApplicationRecord(getActivity().getApplication().getPackageName());

                if (Utils.writeTag(getActivity(),new NdefMessage(mimeRecord, aar), detected)) {
                    Toast.makeText(getActivity(), "Card written to tag successfully", Toast.LENGTH_LONG).show();
                }
            }catch ( Exception ex){
                ex.printStackTrace();
                Toast.makeText(getActivity(), "Card wasn't written to tag", Toast.LENGTH_LONG).show();
            }
            mWriteMode = false;
        }
    }

    private void callToCardHolder( Card card ){
        if( null != card.getPhone() ) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + card.getPhone()));
            startActivity(callIntent);
        }
    }

    private void displayCard(Card card) {
        fab.setImageBitmap(BitmapFactory.decodeResource(getActivity().getResources(),card.isOwn() ? R.drawable.ic_nfc : R.drawable.ic_call));

        holderName.setText(card.getName());
        holderRole.setText( card.getRole()+" at "+card.getCompany());
        holderPhone.setText( card.getPhone() );
        holderAddress.setText( card.getAddress() );
        holderEmail.setText( card.getEmail() );
    }
}
